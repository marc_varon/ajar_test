<?php

namespace Tests\Feature\Properties;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;

class ImportCsvData extends TestCase
{
    public function testMissingCsvDataController()
    {
        $this->json('POST', 'api/properties/1/import_data_from_csv')
            ->assertStatus(400);
    }


    public function testBuildCsvDataController()
    {
        $path          = storage_path('csv/sheet1.csv');
        $original_name = 'sheet1.csv';
        $mime_type     = 'text/csv';
        $size          = filesize(storage_path('csv/sheet1.csv'));
        $error         = null;
        $test          = true;

        $csvFile = new UploadedFile($path, $original_name, $mime_type, $size, $error, $test);

        $this->json('POST', 'api/properties/1/import_data_from_csv', ['csv' => $csvFile])
            ->assertStatus(201)
            ->assertJsonStructure(
                [
                    'success_records',
                    'error_records',
                ],
                [
                    'success_records' => 148,
                    'error_records' => 0
                ]
            );

    }
}
