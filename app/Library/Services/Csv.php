<?php
namespace App\Library\Services;

/**
 * Class Csv
 * @package App\Library\Services
 */
class Csv
{

    /**
     * @param string $input
     * @param int $headerRowKey
     * @param string $delimiter
     * @param string $enclosure
     * @param string $escape
     * @return array
     */
    public function stringCsvToArray(string $input, int $headerRowKey = 0, string $delimiter = ',', string $enclosure = '""', string $escape = '')
    {
        $csvArray = [];

        //get from csv
        $rows = array_map(function ($item) use ($delimiter, $enclosure, $escape) {
            return str_getcsv($item, $delimiter, $enclosure, $escape);
        }, explode("\n", $input));

        if (count($rows) > $headerRowKey) {
            //removing useless data
            for ($i = 0; $i < $headerRowKey; $i++) {
                array_shift($rows);
            }

            //get header
            $header = array_shift($rows);
            $header = array_map(function ($item) {
                //filter header
                return str_slug($item, '_');
            }, $header);

            //construct array
            foreach ($rows as $row) {
                if (count($header) === count($row)) {
                    $row = array_map(function ($item) {
                        return trim($item);
                    }, $row);
                    $csvArray[] = array_combine($header, $row);
                }
            }
        }

        return $csvArray;
    }
}