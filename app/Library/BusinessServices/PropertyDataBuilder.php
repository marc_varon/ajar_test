<?php

namespace App\Library\BusinessServices;

use App\Models\Lease;
use App\Models\LeaseFrequency;
use App\Models\Property;
use App\Models\Tenant;
use App\Models\Unit;
use App\Models\UnitType;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Class PropertyDataBuilder
 * @package App\Library\BusinessServices
 */
class PropertyDataBuilder
{
    /**
     * @var Property
     */
    private $property;
    private $errorLog;
    private $successLog;

    function __construct()
    {
        //Initiate Logger
        $logPath = app_path('../storage/logs/');
        $this->errorLog = new Logger('errorLog');
        $this->errorLog->pushHandler(new StreamHandler($logPath . date('Ymd') . '_property_data_builder_error.log', Logger::ERROR));
        $this->successLog = new Logger('successLog');
        $this->successLog->pushHandler(new StreamHandler($logPath . date('Ymd') . '_property_data_builder_success.log', Logger::INFO));
    }

    /**
     * @param Property $property
     */
    public function setProperty(Property $property)
    {
        $this->property = $property;
    }


    /**
     * @param array $csvArray
     * @return \stdClass
     */
    public function buildFromCsvArray(array $csvArray)
    {
        $result = new \stdClass();
        $result->ok = 0;
        $result->ko = 0;

        //normalize csv headers
        $unitMap = [
            'unit_number' => 'unit_number',
            'unit_type' => 'unit_type',
            'size' => 'size',
            'bathrooms' => 'bathrooms',
            'bedrooms' => 'bedrooms',
        ];
        $tenantMap = [
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'email' => 'email',
            'phone' => 'phone',
            'national_id' => 'national_id',
        ];
        $landlordMap = [
            'landlord' => 'tenant_or_landlord',
        ]; //needn't to be populated
        $leaseMap = [
            'start_date' => 'lease_start_date',
            'start_online_collection' => 'start_online_collection',
            'end_date' => 'lease_end_date',
            'rent' => 'rent',
            'frequency' => 'frquency',
        ];

        //Keep this in memory
        $unitTypes = [];
        $leaseFrequencies = [];

        foreach ($csvArray as $key => $dataRow) {
            $csvFormatDate = 'd-m-y';

            //Check valid Unit Type
            $unitTypeCode = strtoupper($dataRow[$unitMap['unit_type']]);
            $unitType = isset($unitTypes[$unitTypeCode]) ? $unitTypes[$unitTypeCode] : UnitType::where('code', '=', $unitTypeCode)->first();
            if (!$unitType) {
                $result->ko++;
                $this->errorLog->error('Invalid unit type; Record: ' . implode('#', $dataRow));
            } else {
                //Keep Unit Type
                $unitTypes[$unitType->code] = $unitType;

                //Check valid Lease Frequency
                $leaseFrequencyCode = strtoupper($dataRow[$leaseMap['frequency']]);
                $leaseFrequency = isset($leaseFrequencies[$leaseFrequencyCode]) ? $leaseFrequencies[$leaseFrequencyCode] : LeaseFrequency::where('code', '=', $leaseFrequencyCode)->first();
                if (!$leaseFrequency) {
                    $result->ko++;
                    $this->errorLog->error('Invalid lease frequency; Record: ' . implode('#', $dataRow));
                } else {
                    //Keep Lease Frequency
                    $leaseFrequencies[$leaseFrequency->code] = $leaseFrequency;

                    //Insert or update Tenant
                    try {
                        $tenant = Tenant::updateOrCreate(
                            [
                                'national_id' => $dataRow[$tenantMap['national_id']],
                            ],
                            [
                                'first_name' => $dataRow[$tenantMap['first_name']],
                                'last_name' => $dataRow[$tenantMap['last_name']],
                                'email' => $dataRow[$tenantMap['email']],
                                'phone' => $dataRow[$tenantMap['phone']],
                                'national_id' => $dataRow[$tenantMap['national_id']],
                            ]
                        );
                    } catch (\Illuminate\Database\QueryException $exception) {
                        $errorInfo = $exception->errorInfo;
                        $result->ko++;
                        $this->errorLog->error('Error on update or create tenant;' . end($errorInfo) . '; Record: ' . implode('#', $dataRow));
                        continue;
                    }

                    //Insert or update Unit
                    try {
                        $unit = Unit::UpdateOrCreate(
                            [
                                'unit_number' => $dataRow[$unitMap['unit_number']],
                            ],
                            [
                                'unit_number' => $dataRow[$unitMap['unit_number']],
                                'size' => $dataRow[$unitMap['size']],
                                'bathrooms' => $dataRow[$unitMap['bathrooms']],
                                'bedrooms' => $dataRow[$unitMap['bedrooms']],
                                'unit_type_id' => $unitType->id,
                                'tenant_id' => $tenant->id,
                                'property_id' => $this->property->id,
                            ]);
                    } catch (\Illuminate\Database\QueryException $exception) {
                        $errorInfo = $exception->errorInfo;
                        $result->ko++;
                        $this->errorLog->error('Error on update or create unit;' . end($errorInfo) . '; Record: ' . implode('#', $dataRow));
                        continue;
                    }

                    //Check if unit has already an active lease
                    if ($oldActiveLease = Lease::where([['unit_id', '=', $unit->id], ['active', '=', 1]])->first()) {
                        $oldActiveLease->active = 0;
                        $oldActiveLease->save();
                    }

                    //Insert Lease
                    $startDate = \DateTime::createFromFormat($csvFormatDate, $dataRow[$leaseMap['start_date']]);
                    $endDate = \DateTime::createFromFormat($csvFormatDate, $dataRow[$leaseMap['end_date']]);
                    $startCollectionDate = \DateTime::createFromFormat($csvFormatDate, $dataRow[$leaseMap['start_online_collection']]);
                    try {
                        Lease::create([
                            'start_date' => $startDate->format('Y-m-d'),
                            'start_online_collection' => $startCollectionDate->format('Y-m-d'),
                            'end_date' => $endDate->format('Y-m-d'),
                            'rent' => str_replace(',', '', $dataRow[$leaseMap['rent']]),
                            'frequency_id' => $leaseFrequency->id,
                            'active' => 1,
                            'unit_id' => $unit->id,
                        ]);
                    } catch (\Illuminate\Database\QueryException $exception) {
                        $errorInfo = $exception->errorInfo;
                        $result->ko++;
                        $this->errorLog->error('Error on update or create unit;' . end($errorInfo) . '; Record: ' . implode('#', $dataRow));
                        continue;
                    }

                    //success
                    $result->ok++;
                    $this->successLog->info('Record parsed; ' . implode('#', $dataRow));
                }
            }
        }

        return $result;
    }
}