<?php

namespace App\Http\Controllers\Properties;

use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Http\Request;
use App\Library\Services\Csv;
use App\Library\BusinessServices\PropertyDataBuilder;

class ImportDataFromCsvController extends Controller
{


    /**
     * @param Request $request
     * @param Property $property
     * @param Csv $csvServiceInstance
     * @param PropertyDataBuilder $propertyDataBuilder
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Property $property, Csv $csvServiceInstance, PropertyDataBuilder $propertyDataBuilder)
    {
        //check expected file
        if (!$request->hasFile('csv')) {
            return response()->json(['message' => 'Bad Request'], 400);
        }

        //transform to array
        $csvString = file_get_contents($request->file('csv'));
        $csvArray = $csvServiceInstance->stringCsvToArray($csvString, 1);

        //import data
        $propertyDataBuilder->setProperty($property);
        $result = $propertyDataBuilder->buildFromCsvArray($csvArray);

        return response()->json(['success_records' => $result->ok, 'error_records' => $result->ko], 201);
    }
}
