<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ValidateCsvFile
{
    private $validContentTypes = [
        'text/csv',
        'application/vnd.ms-excel',
    ];
    private $maxFileSize = 2 * 1024 * 1024;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $uploadedFiles = $request->file();
        $file = current($uploadedFiles);

        // Limit to only one file
        if (count($uploadedFiles) !== 1) {
            return response()->json(['message' => 'Bad Request'], 400);
        }

        // Validate Mime Type
        if (!in_array($file->getClientMimeType(), $this->validContentTypes)) {
            return response()->json(['message' => 'Unsupported Media Type'], 415);
        }

        // Validate max size
        if ($file->getClientSize() > $this->maxFileSize) {
            return response()->json(['message' => 'File too large, you can upload files up to ' . $this->maxFileSize . 'B'], 400);
        }

        return $next($request);
    }
}
