<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\Services\Csv;

class CsvServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\Csv', function ($app) {
            return new Csv();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Csv::class];
    }
}
