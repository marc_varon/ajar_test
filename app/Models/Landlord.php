<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Landlord extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * Get the properties for the landlord.
     */
    public function properties()
    {
        return $this->hasMany('App\Property');
    }
}
