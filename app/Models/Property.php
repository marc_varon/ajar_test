<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $fillable = [
        'landlord_id',
    ];

    /**
     *  Get the landlord that owns the property
     */
    public function landlord()
    {
        return $this->belongsTo('App\Landlord');
    }

    /**
     * Get the units for the lease
     */
    public function units()
    {
        return $this->hasMany('App\Unit');
    }
}
