<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'unit_number',
        'size',
        'bathrooms',
        'bedrooms',
        'property_id',
        'tenant_id',
        'unit_type_id',
    ];

    /**
     *  Get the property of the unit
     */
    public function property()
    {
        return $this->belongsTo('App\Property');
    }

    /**
     *  Get the tenant of the unit
     */
    public function tenant()
    {
        return $this->belongsTo('App\Tenant');
    }

    /**
     *  Get the lease of the unit
     */
    public function leases()
    {
        return $this->hasMany('App\Lease');
    }

    /**
     *  Get the unit type of the unit
     */
    public function unit_type()
    {
        return $this->belongsTo('App\UnitType');
    }
}
