<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lease extends Model
{
    protected $fillable = [
        'start_date',
        'start_online_collection',
        'end_date',
        'rent',
        'frequency_id',
        'unit_id',
        'active',
    ];

    /**
     *  Get the lease frequency of the property
     */
    public function lease_frequency()
    {
        return $this->belongsTo('App\LeaseFrequency');
    }

    /**
     * Get the units for the lease
     */
    public function unit()
    {
        return $this->belongsTo('App\Unit');
    }
}
