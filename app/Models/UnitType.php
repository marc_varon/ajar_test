<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitType extends Model
{
    protected $fillable = [
        'code',
    ];

    /**
     * Get the units for the unit type
     */
    public function units()
    {
        return $this->hasMany('App\Unit');
    }
}
