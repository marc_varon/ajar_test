<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'national_id',
    ];

    /**
     * Get the units for the tenant
     */
    public function units()
    {
        return $this->hasMany('App\Unit');
    }
}
