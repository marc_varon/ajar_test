<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaseFrequency extends Model
{
    protected $fillable = [
        'code',
    ];

    /**
     * Get the leases for the lease frequency
     */
    public function leases()
    {
        return $this->hasMany('App\Lease');
    }
}
