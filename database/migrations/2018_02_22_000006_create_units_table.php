<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unit_number');
            $table->integer('size');
            $table->integer('bathrooms');
            $table->integer('bedrooms');
            $table->integer('property_id')->unsigned();
            $table->integer('tenant_id')->unsigned();
            $table->integer('unit_type_id')->unsigned();
            $table->timestamps();

            //relationships
            $table->foreign('property_id')->references('id')->on('properties');
            $table->foreign('tenant_id')->references('id')->on('tenants');
            $table->foreign('unit_type_id')->references('id')->on('unit_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('units');
    }
}
