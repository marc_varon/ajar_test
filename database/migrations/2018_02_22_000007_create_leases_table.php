<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leases', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->date('start_online_collection');
            $table->date('end_date');
            $table->decimal('rent', 18, 2);
            $table->integer('frequency_id')->unsigned();
            $table->integer('unit_id')->unsigned();
            $table->boolean('active');
            $table->timestamps();

            //Relationships
            $table->foreign('frequency_id')->references('id')->on('lease_frequencies');
            $table->foreign('unit_id')->references('id')->on('tenants');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leases');
    }
}
