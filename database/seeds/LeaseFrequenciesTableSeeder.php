<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
Use App\Models\LeaseFrequency;

class LeaseFrequenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaseFrequency::query()->updateOrCreate(['code' => 'MONTHLY'], [
            'code' => 'MONTHLY',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        LeaseFrequency::query()->updateOrCreate(['code' => 'QUARTERLY'], [
            'code' => 'QUARTERLY',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        LeaseFrequency::query()->updateOrCreate(['code' => 'ANNUALLY'], [
            'code' => 'ANNUALLY',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
