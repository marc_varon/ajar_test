<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LandlordsTableSeeder::class);
        $this->call(PropertiesTableSeeder::class);
        $this->call(LeaseFrequenciesTableSeeder::class);
        $this->call(UnitTypesTableSeeder::class);
    }
}
